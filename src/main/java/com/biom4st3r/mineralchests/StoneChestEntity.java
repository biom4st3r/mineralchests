package com.biom4st3r.mineralchests;

import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.ChestBlockEntity;
import net.minecraft.container.Container;
import net.minecraft.container.GenericContainer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.text.TextComponent;
import net.minecraft.text.TranslatableTextComponent;
import net.minecraft.util.DefaultedList;
import net.minecraft.world.BlockView;
import java.util.List;

import java.util.Iterator;



public class StoneChestEntity extends ChestBlockEntity {

    private DefaultedList<ItemStack> inventory;

    @Override
    public int getInvSize() {
        return 27;
    }

    @Override
    public void onInvClose(PlayerEntity playerEntity_1) {
        if (!playerEntity_1.isSpectator()) {
            --this.viewerCount;
            this.onInvOpenOrClose();
        }

    }

    @Override
    protected TextComponent getContainerName() {
        return new TranslatableTextComponent("container.chest", new Object[0]);
    }

    @Override
    public boolean isInvEmpty() {
        Iterator var1 = this.inventory.iterator();

        ItemStack itemStack_1;
        do {
            if (!var1.hasNext()) {
                return true;
            }

            itemStack_1 = (ItemStack)var1.next();
        } while(itemStack_1.isEmpty());

        return false;
    }

    @Override
    public boolean onBlockAction(int int_1, int int_2) {
        if (int_1 == 1) {
            this.viewerCount = int_2;
            return true;
        } else {
            return super.onBlockAction(int_1, int_2);
        }
    }

    @Override
    protected void onInvOpenOrClose() {
        Block block_1 = this.getCachedState().getBlock();
        if (block_1 instanceof StoneChest) {
            this.world.addBlockAction(this.pos, block_1, 1, this.viewerCount);
            this.world.updateNeighborsAlways(this.pos, block_1);
        }
    }

    @Override
    protected void setInvStackList(DefaultedList<ItemStack> defaultedList_1) {
        this.inventory = defaultedList_1;
    }

    public BlockEntity createBlockEntity(BlockView blockView_1) {
        return new StoneChestEntity();
    }

    @Override
    protected Container createContainer(int int_1, PlayerInventory playerInventory_1) {
        return new GenericContainer.Generic9x3(int_1, playerInventory_1, this);
    }


}
