package com.biom4st3r.mineralchests;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.EntityCategory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.block.BlockItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class MineralChestsMod implements ModInitializer {

    public static final String MODID = "biom4st3rmineralchests";

    public static final Block StoneChest =
            new Block(FabricBlockSettings.of(Material.STONE).strength(5f,5f).build());

    BlockEntityType<StoneChestEntity> stoneChestEntity = BlockEntityType.Builder.create(StoneChestEntity::new).build(null);
    public void onInitialize() {
        Registry.ITEM.register(
                new Identifier(MODID,"stonechest"),
                new BlockItem(StoneChest, new Item.Settings().stackSize(64).itemGroup(ItemGroup.MISC)));
        Registry.BLOCK.register(new Identifier(MODID,"stonechest"),StoneChest);
        Registry.register(Registry.BLOCK_ENTITY,new Identifier(MODID,"stonechest"),stoneChestEntity);
    }
}
