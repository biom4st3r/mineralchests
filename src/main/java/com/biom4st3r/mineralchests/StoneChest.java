package com.biom4st3r.mineralchests;


import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.ChestBlockEntity;
import net.minecraft.block.enums.ChestType;
import net.minecraft.container.NameableContainerProvider;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.property.Properties;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;


public class StoneChest extends ChestBlock
{
    @Override
    public BlockEntity createBlockEntity(BlockView blockView) {
        return new StoneChestEntity();
    }

    @Override
    public boolean activate(BlockState blockState_1, World world_1, BlockPos blockPos_1, PlayerEntity playerEntity_1, Hand hand_1, BlockHitResult blockHitResult_1) {
        if (world_1.isClient) {
            return true;
        } else {
            NameableContainerProvider nameableContainerProvider_1 = this.createContainerProvider(blockState_1, world_1, blockPos_1);
            if (nameableContainerProvider_1 != null) {
                playerEntity_1.openContainer(nameableContainerProvider_1);
                playerEntity_1.incrementStat(this.getOpenStat());
            }

            return true;
        }
    }

    protected StoneChest(Settings settings)
    {
        super(settings);
        this.setDefaultState((BlockState)((BlockState)((BlockState)((BlockState)this.stateFactory.getDefaultState()).with(Properties.FACING_HORIZONTAL, Direction.NORTH)).with(CHEST_TYPE, ChestType.SINGLE)));
    }

    @Override
    public void onPlaced(World world, BlockPos blockPos, BlockState blockState, LivingEntity le, ItemStack iS) {
        if (iS.hasDisplayName()) {
            BlockEntity blockEntity = world.getBlockEntity(blockPos);
            if (blockEntity instanceof StoneChestEntity) {
                ((StoneChestEntity)blockEntity).setCustomName(iS.getDisplayName());
            }
        }

    }
}
